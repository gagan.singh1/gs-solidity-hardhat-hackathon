//SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.9.0;

import "hardhat/console.sol";
import "./Safemath.sol";
import "./ERC20.sol";
import "./Stock.sol";


/**
 * @title An attempt at an on-chain (very basic) stock/token trading
 *
 * @dev List a symbol / Buy & Sell shares
 * 
 */
contract Exchange {
    using SafeMath for uint256;

    string public exchangeName;
    uint256 brokerCommission = 0.001 ether;
    uint256 priceChange = 0.01 ether;
    uint256 allowedDeviation = 0.10 ether;
    uint256 public stockCount;
    
    mapping(string => address) public stockOwnership;
    mapping(string => Stock) public stocks;
    mapping(string => uint256) public availableInMarket;

    // IERC20 Stock;

    struct MarketOffer {
        address payable seller;
        uint256 offeredShares;
        uint256 ask;
        uint256 bid;
        address buyer;
        uint256 purchaseAmount;
        uint256 shareTransfer;
    }

    mapping(string => uint) index;
    mapping(string => MarketOffer) public secondMarket;

    // An address type variable is used to store ethereum accounts.
    address payable public owner;

    constructor(string memory _name) {
        exchangeName = _name;
        owner = payable(msg.sender);
    }


    /**
    * @dev Throws if called by any account other than the owner.
    */
    modifier onlyOwner() {
        require(isOwner());
        _;
    }

    /**
    * @return true if `msg.sender` is the owner of the contract.
    */
    function isOwner() public view returns(bool) {
        return msg.sender == owner;
    }

    function deposit() external payable {
        owner.transfer(msg.value);
    }

    function transferToAddr(address payable addr) external payable {
        addr.transfer(msg.value);
    }

    /**
     * @dev Initial Public Offering.
     *
     * The `external` modifier makes a function *only* callable from outside
     * the contract. Only the owner of the contract has the rights to list new Stock.
     */
    function listStock(string memory _name, string memory _symbol, uint256 _totalshares) external payable {
        require(index[_symbol] == 0, "Stock already listed on Exchange");
        stockCount += 1;
        Stock newStock = new Stock(_name, _symbol, _totalshares);
        stocks[_symbol] = newStock;
        index[_symbol] = stockCount;
    }

    /**
     * @dev Buying during the owner/company IPO event.
     *
     * The `external` modifier makes a function *only* callable from outside
     * the contract. Only the owner of the contract has the rights to list new Stock.
     */
    function buy(string memory _symbol, uint256 _numshares) external payable returns (bool) {
        Stock theStock = stocks[_symbol];
        uint256 price = theStock.marketPrice();
        uint256 totalValue = _numshares * price;
        require(msg.value == totalValue + brokerCommission, "Not enough money sent for ipo purchase");
        require(_numshares <= theStock.outstanding(), "not enough outstanding shares. Try secondary market.");
        theStock.transfer(msg.sender, _numshares);
        return true;
    }

    /**
     * @dev Selling in the Secondary Market. Offer some Stock for sale.
     **/
    function sellInMarket(string memory _symbol, uint256 _numshares, uint _askPrice) external payable {
        require(msg.value >= brokerCommission, "Must pay broker fee.");
        Stock theStock = stocks[_symbol];
        require (_numshares <= theStock.balanceOf(msg.sender), "Cannot offer more than you own");
        if (_askPrice > theStock.marketPrice()) {
            require (_askPrice - theStock.marketPrice() <= allowedDeviation, "Charging too much!");
        }
        MarketOffer storage mktOff = secondMarket[_symbol];
        mktOff.ask = _askPrice;
        mktOff.offeredShares = _numshares;
        mktOff.seller = payable(msg.sender);
        availableInMarket[_symbol] = _numshares;
    }

    /**
     * @dev Buying from the Secondary Market. Offering a price.
     */
    function buyFromMarket(string memory _symbol, uint256 _numshares, uint256 _bid) external payable {
        // Stock theStock = stocks[_symbol];
        require(msg.value >= brokerCommission, "Must pay broker fee.");
        MarketOffer storage mktOff = secondMarket[_symbol];
        require(mktOff.offeredShares >= _numshares, "not enough shares in the market");
        require(msg.value >= brokerCommission, "Not enough funds");
        mktOff.shareTransfer = _numshares;
        mktOff.purchaseAmount = _numshares * _bid;
        mktOff.buyer = msg.sender;
        // -todo- ideally an event should fire here
    }

    /**
     * @dev Accepting/Approving the sale. No payment required.
     *
     **/
    function approveMarketSale(string memory _symbol) external {
        MarketOffer storage mktOff = secondMarket[_symbol];
        require(msg.sender == mktOff.seller, "Only the original seller can approve the purchase");
        Stock theStock = stocks[_symbol];
        theStock.approve(msg.sender, mktOff.buyer, mktOff.shareTransfer);
        //// -todo- ideally an event should fire here 
    }

    /**
     * @dev Complete the sale. Payment required to pay the seller.
     *
     **/
    function completeMarketSale(string memory _symbol) external payable {
        (Stock theStock, MarketOffer storage mktOff) = _getStockAndMarketOffer(_symbol);
        require(msg.sender == mktOff.buyer, "Only the buyer can complete the purchase");
        require(msg.value == mktOff.purchaseAmount, "Must pay the full amount");
        uint256 toTransfer = mktOff.shareTransfer;
        theStock.transferFrom(mktOff.seller, msg.sender, toTransfer);
        (bool sent, /*bytes memory data*/) = mktOff.seller.call{value: msg.value}("");
        require(sent, "Failed to send Ether");
        availableInMarket[_symbol] -= toTransfer;
        mktOff.offeredShares -= toTransfer;
    }
        
    /**
     * @dev View to query outstanding shares left from the IPO sale
     *
     **/
    function getOutstandingShares(string memory _symbol) external view returns (uint256) {
        Stock theStock = stocks[_symbol];
        return theStock.outstanding();
    }

    /**
     * @dev View to get market price of a stock
     *
     **/
    function getMarketPrice(string memory _symbol) external view returns (uint) {
        Stock theStock = stocks[_symbol];
        return theStock.marketPrice();
    }

    /**
     * @dev View to see available shares in the market
     *
     **/
    function getSharesInMarket(string memory _symbol) external view returns (uint256) {
        return availableInMarket[_symbol];
    }

    /**
     * @dev View to get the balance of a possible shareholder
     *
     **/
    function getBalance(string memory _symbol, address _shareholder) external view returns (uint256) {
        Stock theStock = stocks[_symbol];
        return theStock.balanceOf(_shareholder);
    }


    function _getStockAndMarketOffer(string memory _symbol) internal view returns (Stock, MarketOffer storage) {
        MarketOffer storage mktOff = secondMarket[_symbol];
        Stock theStock = stocks[_symbol];
        return (theStock, mktOff);
    }
}