// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IERC20.sol";
import "./Safemath.sol";
import "hardhat/console.sol";


/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
 * Originally based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract ERC20 is IERC20 {
    using SafeMath for uint256;

    mapping (address => uint256) public override balanceOf;

    mapping (address => mapping (address => uint256)) private allowances;

    mapping (address => uint) shareholders;

    string public name;
    string public symbol;
    uint256 public override totalSupply;
    uint256 public outstanding;

    uint8 public decimals = 18;

    /**
     * @dev Sets the values for {name} and {symbol}, initializes {decimals} with
     * a default value of 18.
     *
     * To select a different value for {decimals}, use {_setupDecimals}.
     *
     * All three of these values are immutable: they can only be set once during
     * construction.
     */
    constructor (string memory _name, string memory _symbol, uint256 _totalshares) {
        name = _name;
        symbol = _symbol;
        outstanding = _totalshares;
    }

    /**
    * @dev Function to check the amount of tokens that an owner allowed to a spender.
    * @param _owner address The address which owns the funds.
    * @param _spender address The address which will spend the funds.
    * @return A uint256 specifying the amount of tokens still available for the spender.
    */
    function allowance(address _owner, address _spender) external view virtual override returns (uint256) {
        return allowances[_owner][_spender];
    }

    /**
    * @dev Transfer token for a specified address
    * @param _to The address to transfer to.
    * @param _amount The amount to be transferred.
    */
    function transfer(address _to, uint _amount) external virtual override returns (bool) {
        require(_amount <= balanceOf[msg.sender]);
        require(_to != address(0));
        balanceOf[msg.sender] -= _amount;
        balanceOf[_to] += _amount;
        outstanding -= _amount;
        emit Transfer(msg.sender, _to, _amount);
        return true;
    }
  

  /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   * @param _spender The address which will spend the funds.
   * @param _amount The amount of tokens to be spent.
   */
    function approve(address _spender, uint256 _amount) external virtual override returns (bool) {
        require(_spender != address(0));
        allowances[msg.sender][_spender] = _amount;
        emit Approval(msg.sender, _spender, _amount);
        return true;
    }

    /**
    * @dev Transfer tokens from one address to another
    * @param from address The address which you want to send tokens from
    * @param to address The address which you want to transfer to
    * @param amount uint256 the amount of tokens to be transferred
    */
    function transferFrom(address from, address to, uint256 amount) external override returns (bool) {
        require(balanceOf[from] >= amount, "Sender doesn't have enough shares. BUG!");
        require(to != address(0), "Receipient address is null!");
        require(allowances[from][to] >= amount, "Allowance lower than the requested amount");
        allowances[from][to] -= amount;
        balanceOf[from] -= amount;
        balanceOf[to] += amount;
        console.log("== Trade Successful!! ==");
        emit Transfer(from, to, amount);
        return true;
    }


  /**
   * @dev Internal function that mints an amount of the token and assigns it to
   * an account. This encapsulates the modification of balances such that the
   * proper events are emitted.
   * @param account The account that will receive the created tokens.
   * @param amount The amount that will be created.
   */
  function _mint(address account, uint256 amount) internal {
        require(account != address(0));
        totalSupply = totalSupply.add(amount);
        balanceOf[account] = balanceOf[account].add(amount);
        emit Transfer(address(0), account, amount);
  }

    // /**
    // * @dev Internal function that burns an amount of the token of a given
    // * account. WE DONT WANT TO BURN ANYTHING, YET.
    // * @param account The account whose tokens will be burnt.
    // * @param amount The amount that will be burnt.
    // */
    // function burn(address account, uint256 amount) internal {
    //     require(account != address(0));
    //     require(amount <= balanceOf[account]);

    //     totalSupply = totalSupply.sub(amount);
    //     balanceOf[account] = balanceOf[account].sub(amount);
    //     emit Transfer(account, address(0), amount);
    // }

    /**
    * @dev Internal function that burns an amount of the token of a given
    * account.
    * @param owner The account whose tokens will be taken away.
    * @param spender The amount that will be burnt.
    */
    function _approve(address owner, address spender, uint256 amount) internal virtual returns (bool) {
        allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
        return true;
    }
}

  