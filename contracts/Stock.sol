//SPDX-License-Identifier: UNLICENSED

pragma solidity >=0.6.0 <0.9.0;

import "hardhat/console.sol";
import "./Safemath.sol";
import "./ERC20.sol";


/**
 * @title Stock token.
 *
 * @dev Stock is a cusomtized ERC20 token, but follows the ERC20 API standard as closely as possible
 * https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
 * Originally based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract Stock is ERC20 {
    using SafeMath for uint256;


    uint256 defaultPrice  = 0.50 ether;
    uint256 priceChange = 0.001 ether;
    uint256 public ipoPrice;
    uint256 public marketPrice;

    address payable public owner;

    /**
    * @dev Throws if called by any account other than the owner.
    */
    modifier onlyOwner() {
        require(isOwner());
        _;
    }

    /**
    * @return true if `msg.sender` is the owner of the contract.
    */
    function isOwner() public view returns(bool) {
        return msg.sender == owner;
    }

    /**
    * @dev  This will invoke the ERC20 constructor as well.
            The totalSupply is assigned to transaction sender, which is the account
            that is deploying/creating the contract.
    */
    constructor(string memory _name, 
                string memory _symbol, 
                uint256 _supply) ERC20(_name, _symbol, _supply) {
        ipoPrice = defaultPrice;
        marketPrice = defaultPrice;
        owner = payable(msg.sender);
        _mint(owner, _supply);
    }
    

    /**
    * @dev Transfer token to a specified address. Increase price if there's demand.
    * @param to The address to transfer to.
    * @param amount The amount to be transferred.
    */
    function transfer(address to, uint amount) external virtual override returns (bool) {
        require(amount <= balanceOf[msg.sender]);
        require(to != address(0));
        balanceOf[owner] -= amount;
        balanceOf[to] += amount;
        outstanding -= amount;
        emit Transfer(msg.sender, to, amount);
        if(outstanding < totalSupply / 2) {
            _setMarketPrice(marketPrice + priceChange);
        }
        if(amount > totalSupply / 2) {
            _setMarketPrice(marketPrice + priceChange);
        }
        return true;
    }



    /**
    * @dev Owner can set/change market price
    * @param _price The address to transfer to.
    */
    function setMarketPrice(uint _price) external onlyOwner {
        _setMarketPrice(_price);
    }
    

    /**
    * @dev set market price token to a specified address
    * @param _price The address to transfer to.
    */
    function _setMarketPrice(uint _price) internal {
        marketPrice = _price;
    }

    /**
    * @dev The ONLY owner is Exchange. It can approve allowances.
    * 
    */
    function approve(address from, address to, uint256 amount) external onlyOwner {
        _approve(from, to, amount);
    }

}