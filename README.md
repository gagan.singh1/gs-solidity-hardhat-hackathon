# Solidity-hardhat-hackathon
getting hands dirty with solidity

## Getting started
Follow the hardhat setup guide:
https://hardhat.org/tutorial/setting-up-the-environment.html

After that..

```npx hardhat test```


***

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Name
Solidity Hardhat Hackathon project  


## License
MIT

