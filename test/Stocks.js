const { expect } = require("chai");
const { hardhatArguments } = require("hardhat");
const hardhatConfig = require("../hardhat.config");

describe("Full StockExchange contract Test", function () {
    let owner;
    let hardhatExchange;
    let stockCount;

    beforeEach(async function () {
        [owner, addr1, addr2] = await ethers.getSigners();
    
        const Stocks = await ethers.getContractFactory("Exchange");
    
        hardhatExchange = await Stocks.deploy("NYSE");

        stockCount = await hardhatExchange.stockCount();

        await hardhatExchange.listStock("Apple Inc", "AAPL", 1000);
        await hardhatExchange.listStock("Netflix", "NFLX", 5000);
        await hardhatExchange.listStock("Starbucks", "SBX", 1000);
    });

    describe("Exchange contract", function () {
        it("DEPLOYMENT should create an Exchange named NYSE", async function () {
            expect(await hardhatExchange.exchangeName()).to.equal("NYSE");
        });


        it("LISTING twice is not allowed and the count should remain at 3", async function () {
            await expect(hardhatExchange.listStock("Apple Inc", "AAPL", 1000)).to.be.revertedWith("Stock already listed on Exchange");
           
            expect(await hardhatExchange.stockCount()).to.equal(stockCount + 3);
        });
     

        it("Buying selling on the market should change balances", async function () {
            var options = {value: ethers.utils.parseEther("50.001")};
            console.log("=== buy(AAPL, 100, %s) ====", options);
            await hardhatExchange.connect(addr1).buy("AAPL", 100, options);
            
            expect(await hardhatExchange.getOutstandingShares("AAPL")).to.equal(900);
            
            var options = {value: ethers.utils.parseEther("0.001")};
            await hardhatExchange.connect(addr1).sellInMarket("AAPL", 50, ethers.utils.parseEther("0.51"), options);
            
            expect(await hardhatExchange.availableInMarket("AAPL")).to.equal(50);

            var options = {value: ethers.utils.parseEther("0.001")};
            await hardhatExchange.connect(addr2).buyFromMarket("AAPL", 10, ethers.utils.parseEther("0.60"), options);
            await hardhatExchange.connect(addr1).approveMarketSale("AAPL");
            var options = {value: ethers.utils.parseEther("6.00")};
            await hardhatExchange.connect(addr2).completeMarketSale("AAPL", options);
            
            expect(await hardhatExchange.getBalance("AAPL", addr2.address)).to.equal(10);
            expect(await hardhatExchange.getBalance("AAPL", addr1.address)).to.equal(90);
            
            //expect(await hardhatExchange.getSecondMarketShares("AAPL")).to.equal(50);
            
        });
    });
});